const TableName = process.env.TABLE_NAME;
const appId = parseInt(process.env.APP_ID);
// You'll need to call dynamoClient methods to envoke CRUD operations on the DynamoDB table
const dynamoClient = require("../db");

// The apps routes will uses these service methods to interact with our DynamoDB Table
module.exports = class ListAppService {
  generateParams = () => {
    return {
      TableName,
      Key: { id: appId },
    };
  };

  async getListData() {
    try {
      const listData = await dynamoClient
        .get(this.generateParams())
        .promise();
      return listData.Item;
    } catch (error) {
      return error;
    }
  }

  async getTitle() {
    try {
      const listData = await dynamoClient
        .get(this.generateParams())
        .promise();
      return listData.Item.title;
    } catch (error) {
      return error;
    }
  }

  async changeTitle(title) {
    try {
      const listData = {};
      listData.TableName = TableName;
      listData.Key = { id: appId };
      listData.UpdateExpression = "set #MyTitle = :x";
      listData.ExpressionAttributeNames = {
        "#MyTitle": "title"
      }
      listData.ExpressionAttributeValues = {
        ":x": title
      }

      await dynamoClient.update(listData).promise();

      const updatedTitle = await dynamoClient
        .get(this.generateParams())
        .promise();
      return updatedTitle.Item.title;
    } catch (error) {
      return error;
    }
  }

  async getList() {
    try {
      const listData = await dynamoClient
        .get(this.generateParams())
        .promise();
      return listData.Item.items;
    } catch (error) {
      return error;
    }
  }

  async addToList(item) {
    try {
      const params = {};
  
      params.TableName = TableName;
      params.Key = { id: appId };
      params.UpdateExpression = "SET #attrName = list_append(#attrName, :attrValue)"
      params.ExpressionAttributeNames = {
        "#attrName": "items"
      }
      params.ExpressionAttributeValues = {
        ":attrValue": [item]
      }

      await dynamoClient.update(params).promise();

      const updatedList = await dynamoClient
        .get(this.generateParams())
        .promise();
      return updatedList.Item.items;
    } catch (error) {
      return error;
    }
  }

  async updateItem(index, name) {
    try {
      const params = {};
      let newData = { name: name }
      params.TableName = TableName;
      params.Key = { id: appId };
      params.UpdateExpression = `SET #attrName[${index}] = :attrValue`;
      params.ExpressionAttributeNames = {
        "#attrName": "items"
      };
      params.ExpressionAttributeValues = {
        ":attrValue": newData
      };

      await dynamoClient.update(params).promise();

      const updatedList = await dynamoClient
        .get(this.generateParams())
        .promise();
      return updatedList.Item.items;
    } catch (error) {
      return error;
    }
  }

  async deleteItem(index) {
    try {
      const params = {}
      params.TableName = TableName;
      params.Key = { id: appId };
      params.UpdateExpression = `remove #attrName[${index}]`;
      params.ExpressionAttributeNames = {
        "#attrName": "items"
      }

      await dynamoClient.update(params).promise();

      const updatedList = await dynamoClient
        .get(this.generateParams())
        .promise();
      return updatedList.Item.items;

    } catch (error) {
      return error;
    }
  }
};
